<?php

// Route::get('/', function () {
//     return view('welcome');
// });

//Route::get('/','PagesController@index');

//Route::get('about','PagesController@about');

//Route::get('login','PagesController@login');

// Route::get('about', function () {
//     return view('pages.about');
// });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'HomeController@index');

    Route::resource('users', 'UsersController', ['except' => ['create','store','show']]);
    //Route::get('activate/{code}', 'Auth\AuthController@activateAccount');

    Route::get('/projects', 'ProjectController@index');
    Route::get('/projects/create', 'ProjectController@create');
    Route::post('/projects/create', 'ProjectController@store');
    Route::get('/projects/{projects}/edit', ['uses' => 'ProjectController@edit', 'as' => 'projects.edit']);
    Route::get('/projects-data', ['uses' => 'ProjectController@projectsData', 'as' => 'projects.data']);
    Route::delete('/projects/{projects}', 'ProjectController@destroy');
    Route::put('/projects/{projects}',['uses' => 'ProjectController@update', 'as' => 'projects.update']);

	Route::get('/users-data', ['uses' => 'UsersController@usersData', 'as' => 'users.data']);
    Route::put('/users/{users}/activate', ['uses' => 'UsersController@activateUser', 'as' => 'users.activate']);

    Route::get('auth/verification/{token}', 'Auth\AuthController@getVerification');
});