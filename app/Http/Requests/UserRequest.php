<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (\Auth::check())
            return true;
        
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->users);
        return [
            'email' => 'required|email|max:70|unique:users,email,'.$user->id,
            'first_name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50'
        ];

        // $user = User::find($this->users);
        // switch($this->method())
        // {
        //     case 'GET':
        //     case 'DELETE':
        //     {
        //         return [];
        //     }
        //     case 'POST':
        //     {
        //         return [
        //             'user.name.first' => 'required',
        //             'user.name.last'  => 'required',
        //             'user.email'      => 'required|email|unique:users,email',
        //             'user.password'   => 'required|confirmed',
        //         ];
        //     }
        //     case 'PUT':
        //     case 'PATCH':
        //     {
        //         return [
        //             'user.name.first' => 'required',
        //             'user.name.last'  => 'required',
        //             'user.email'      => 'required|email|unique:users,email,'.$user->id,
        //             'user.password'   => 'required|confirmed',
        //         ];
        //     }
        //     default:break;
        // }
    }
}
