<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected $username = 'username';

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|unique:users|min:3|max:60',
            'email' => 'required|email|max:70|unique:users',
            'password' => 'required|confirmed|min:6',
            'first_name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $verification_token = hash_hmac('sha256', str_random(40) . $request->input('email'), config('app.key'));
        $user = new User;
        $user->username = $request->input('username');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->verification_token = $verification_token;

        if ($user->save()) {
            $data = array(
                'first_name' => $user->first_name,
                'verification_token' => $verification_token,
            );
            \Mail::queue('auth.emails.user-verification', $data, function($message) use ($user) {
                $message->to($user->email, 'Please activate your account.');
                $message->subject('Your Account Verification Link');
            });

            return redirect('login')->with('alert-success', 'Thanks for signing up! Please check your email.');
        }
        else {
            return redirect()->back()->withInput()->with('alert-danger', 'Something went wrong! Please try again');
        }

        // $user = $this->create($request->all());
        // UserVerification::generate($user);

        // UserVerification::send($user, 'Email Verification');

        // \Auth::guard($this->getGuard())->login($user);

        // return redirect($this->redirectPath());
    }

     public function getVerification($verification_token)
    {
        if(!$verification_token)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::where('verification_token', '=', $verification_token)->first();

        if (!$user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->verified = true;
        $user->verification_token = null;
        $user->save();

        //\Session::flash('alert-success', 'Your account has been Activated! You may now login.');  
        return redirect('login')->with('alert-success', 'Your account has been Activated! You may now login.');
    }

    // public function getVerification($token, User $user)
    // {
    //     $user = User::where('activation_code', '=', $token)->first();

    //      if($user->accountIsActive($code)) {
    //         \Session::flash('alert-success', 'Your account has been Activated! You may now login.');  
    //         return redirect()->route('login');
    //     }

    //     if ((!$user->verified) || $user->verification_token != $token) {
    //         return response('Unauthorized.', 401);
    //     }

    //     $user->verification_token = null;

    //     $user->verified = true;

    //     $user->save();

    //     // \Session::flash('alert-success', 'Your account has been Activated! You may now login.');  
    //     //     return redirect()->route('login');
    // }

    // public function login(Request $request)
    // {
    //     $this->validateLogin($request);

    //     // If the class is using the ThrottlesLogins trait, we can automatically throttle
    //     // the login attempts for this application. We'll key this by the username and
    //     // the IP address of the client making these requests into this application.
    //     $throttles = $this->isUsingThrottlesLoginsTrait();

    //     if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
    //         $this->fireLockoutEvent($request);

    //         return $this->sendLockoutResponse($request);
    //     }

    //     $credentials = $this->getCredentials($request);
    //     $credentials = array_add($credentials, 'verified', '1');

    //     if (\Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
    //         return $this->handleUserWasAuthenticated($request, $throttles);
    //     }

    //     // If the login attempt was unsuccessful we will increment the number of attempts
    //     // to login and redirect the user back to the login form. Of course, when this
    //     // user surpasses their maximum number of attempts they will get locked out.
    //     if ($throttles && ! $lockedOut) {
    //         $this->incrementLoginAttempts($request);
    //     }

    //     return $this->sendFailedLoginResponse($request);
    // }

}
