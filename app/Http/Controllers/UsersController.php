<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\User;
use Yajra\Datatables\Datatables;

class UsersController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

 //    /**
	//  * Displays datatables front end view
	//  *
	//  * @return \Illuminate\View\View
	//  */
	// public function getIndex()
	// {
	//     return view('pages.showusers');
	// }

	// *
	//  * Process datatables ajax request.
	//  *
	//  * @return \Illuminate\Http\JsonResponse
	 
	public function usersData()
	{
		//Datatables::of(User::select('id','username','first_name','last_name','email'))->make(true)
		$users = User::select(['id','username','first_name','last_name','email','verified']);

        return Datatables::of($users)
        ->editColumn('verified', function ($id){
            if($id->verified)
                return '<span class="badge badge-success badge-roundless">Verified</span>';
            else
                return '<span class="badge badge-danger badge-roundless">Awaiting Email Confirmation</span>';
        })
        ->addColumn('action', function ($id) {
            return 
            (!$id->verified)?'
            <button id="user-activate" class="btn btn-sm blue" data-toggle="tooltip" data-userid="'.$id->id.'"><i class="fa fa-check-square-o"></i> Activate</button>
            <a href="users/'.$id->id.'/edit" class="btn btn-sm green"><i class="fa fa-edit"></i> Edit</a>
            <button id="user-delete" class="btn btn-sm red" data-toggle="tooltip" data-userid="'.$id->id.'"><i class="fa fa-trash-o"></i> Delete</button>':
            '<a href="users/'.$id->id.'/edit" class="btn btn-sm green"><i class="fa fa-edit"></i> Edit</a>
            <button id="user-delete" class="btn btn-sm red" data-toggle="tooltip" data-userid="'.$id->id.'"><i class="fa fa-trash-o"></i> Delete</button>';
        })

	    ->make(true);
	}

	public function activateUser($id, Request $request)
	{
		 $user = User::findOrFail($id);

	  	if ( $request->ajax() ) {
	        $user->verified = true;
	        $user->verification_token = null;
	        $user->save();
	        $user->touch();
	        	\Session::flash('success', 'User Activated Successfully!'); 
	        return redirect('users');
		}
		//\Session::flash('alert-warning', 'Failed deleting the product!'); 
	    return redirect('users');
	}

	  /**
	   * Display a listing of the resource.
	   *
	   * @return Response
	   */
	  public function index()
	  {
	  	// $users = User::all();
	   //  return view('pages.showusers',compact('users'));
	  	return view('pages.showusers');
	  }

	  /**
	   * Show the form for editing the specified resource.
	   *
	   * @param  int  $id
	   * @return Response
	   */
	  public function edit($id)
	  {
        $user = User::findOrFail($id);
        return View('pages.edituser',compact('user'));
	  }

	  /**
	   * Update the specified resource in storage.
	   *
	   * @param  int  $id
	   * @return Response
	   */
	  public function update($id, UserRequest $request)
	  {
	  	$inputData = $request->all();
		$user = User::findOrFail($id);
		$user->update($inputData);
		$user->touch(); // *updated_at*
		\Session::flash('success', 'User Updated Successfully!');   
	  	return redirect()->back();
	 	// Session::flash('alert-danger', 'danger');
		// Session::flash('alert-warning', 'warning');
		// Session::flash('alert-success', 'success');
		// Session::flash('alert-info', 'info');
	  }

	  /**
	   * Remove the specified resource from storage.
	   *
	   * @param  int  $id
	   * @return Response
	   */
	  public function destroy($id, Request $request)
	  {
	  	 $user = User::findOrFail($id);
	  	// $user->delete();
	  	// \Session::flash('alert-info', 'User Deleted Successfully!');  

	  	if ( $request->ajax() ) {
	        $user->delete( $request->all() );
	        //	\Session::flash('alert-info', 'User Deleted Successfully!'); 
	        return redirect('users');
		}
		//\Session::flash('alert-warning', 'Failed deleting the product!'); 
	    return redirect('users');
	  }

	  // public function store()
	  // {
	  // 	$subject = 'Welcome!';
   //      Mail::send('auth.emails.welcome', ['key' => 'value'], function($message) {
   //        // note: if you don't set this, it will use the defaults from config/mail.php
   //        $message->from('bar@example.com', 'Sender Name');
   //        $message->to('kawaiimash@gmail.com', 'John Smith');
   //      });
	  // }
}
