<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProjectRequest;
use App\Project;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ProjectController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	//$projects = Project::all();
    	return view('pages.projects');
    }

    public function projectsData()
    {
        //Datatables::of(User::select('id','username','first_name','last_name','email'))->make(true)
        $projects = Project::select(['id','project_title','description','active']);

        return Datatables::of($projects)
        ->addColumn('action', function ($id) {
            return '<a href="projects/'.$id->id.'/edit" class="btn btn-sm green"><i class="fa fa-edit"></i> Edit</a>
            <button id="project-delete" class="btn btn-sm red" data-toggle="tooltip" data-projectid="'.$id->id.'"><i class="fa fa-trash-o"></i> Delete</a>';
        })
        ->editColumn('active', function ($id){
            if($id->active == 1)
                return '<span class="badge badge-success badge-roundless">Active</span>';
            else
                return '<span class="badge badge-danger badge-roundless">Inctive</span>';
        })
        ->make(true);
    }

    public function create()
    {
    	return view('pages.createproject');
    }

    public function store(ProjectRequest $request)
    {
    	$input = $request->all();
        $input['active'] = (\Input::has('active')) ? true : false;
    	Project::create($input);

    	\Session::flash('success', 'Project Created Successfully!');
    	return redirect('projects');
    }

    public function edit($id)
    {
    	$project = Project::findOrFail($id);
    	return view('pages.editproject', compact('project'));
    }

    public function update($id, ProjectRequest $request)
    {
        $inputData = $request->all();
        $inputData['active'] = (\Input::has('active')) ? true : false;
        $project = Project::findOrFail($id);
        $project->update($inputData);
        $project->touch(); // *updated_at*
        \Session::flash('success', 'Project Updated Successfully!');   
        return redirect()->back();
    	//$projects
    	// ->setRowClass(function ($user) {
     //            return $user->id % 2 == 0 ? 'alert-success' : 'alert-warning';
     //        })
    }

    public function destroy($id, Request $request)
    {
        $project = Project::findOrFail($id);
        // $user->delete();
        // \Session::flash('alert-info', 'User Deleted Successfully!');  

        if ( $request->ajax() ) {
            $project->delete( $request->all() );
            //  \Session::flash('alert-info', 'User Deleted Successfully!'); 
            return redirect('projects');
        }
        //\Session::flash('alert-warning', 'Failed deleting the product!'); 
        return redirect('projects');
    }

}
