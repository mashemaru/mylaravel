<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	#protected $table = 'projects';

    protected $fillable = [
        'project_title', 'description', 'active',
    ];
}
