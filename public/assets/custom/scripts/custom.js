$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        //scrollX: true,
        pagingType: "full_numbers",
        ajax: '/users-data',
        language: {
            paginate: {
                first: '«',
                previous: '‹',
                next: '›',
                last: '»'
            },
            aria: {
                paginate: {
                    first: 'First',
                    previous: 'Previous',
                    next: 'Next',
                    last: 'Last'
                }
            }
        },
        columns: [{
            data: 'id',
            name: 'id'
        }, {
            data: 'username',
            name: 'username'
        }, {
            data: 'first_name',
            name: 'first_name'
        }, {
            data: 'last_name',
            name: 'last_name'
        }, {
            data: 'email',
            name: 'email'
        }, {
            data: 'verified',
            name: 'verified'
        }, {
            data: 'action',
            name: 'action'
        }]
    });
});

$(document).ready(function() {
    $(document).on('click', '#user-delete', function() {
        var userID = $(this).data('userid');
        deleteUser(userID);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });

    function deleteUser(userID) {
        swal({
            title: "Are you sure",
            text: "you want to delete this User?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/users/" + userID,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                })
                .done(function(data) {
                    sweetAlert({
                            title: "Deleted!",
                            text: "Record Deleted Successfully!",
                            type: "success"
                        },
                        function() {
                            window.location.reload(true);
                        });
                })
                .error(function(data) {
                    swal("Oops", "Something went wrong!", "error");
                });
        });
    }
});

$(document).ready(function() {
    $(document).on('click', '#user-activate', function() {
        var userID = $(this).data('userid');
        activateUser(userID);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });

    function activateUser(userID) {
        swal({
            title: "Are you sure",
            text: "you want to activate this User?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonText: "Confirm!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/users/" + userID+"/activate",
                    type: "POST",
                    data: {
                        '_method': 'PUT'
                    },
                })
                .done(function(data) {
                    sweetAlert({
                            title: "Activated!",
                            text: "User Activated Successfully!",
                            type: "success"
                        },
                        function() {
                            window.location.reload(true);
                        });
                })
                .error(function(data) {
                    swal("Oops", "Something went wrong!", "error");
                });
        });
    }
});

//**************
//**
//** Projects
//**
//**************

$(function() {
    $('#projects-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        //scrollX: true,
        pagingType: "full_numbers",
        ajax: '/projects-data',
        language: {
            paginate: {
                first: '«',
                previous: '‹',
                next: '›',
                last: '»'
            },
            aria: {
                paginate: {
                    first: 'First',
                    previous: 'Previous',
                    next: 'Next',
                    last: 'Last'
                }
            }
        },
        columns: [{
            data: 'id',
            name: 'id'
        }, {
            data: 'project_title',
            name: 'project_title'
        }, {
            data: 'description',
            name: 'description'
        }, {
            data: 'active',
            name: 'active'
        }, {
            data: 'action',
            name: 'action'
        }]
    });
});


$(document).ready(function() {
    $(document).on('click', '#project-delete', function() {
        var projectID = $(this).data('projectid');
        deleteProject(projectID);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });

    function deleteProject(projectID) {
        swal({
            title: "Are you sure",
            text: "you want to delete this Project?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/projects/" + projectID,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                })
                .done(function(data) {
                    sweetAlert({
                            title: "Deleted!",
                            text: "Record Deleted Successfully!",
                            type: "success"
                        },
                        function() {
                            window.location.reload(true);
                        });
                })
                .error(function(data) {
                    swal("Oops", "Something went wrong!", "error");
                });
        });
    }
});