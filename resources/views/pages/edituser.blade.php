@extends('layouts.app')

@section('htmlheader_title')
  Edit User
@endsection

@section('content') <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Blank Page</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Page Layouts</span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> {{ $user->first_name ." ". $user->last_name }} 
                        <small> 's Profile</small>
                    </h3>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
        <div class="col-md-8">
          <div class="box box-info box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Edit User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::model($user, ['route' => ['users.update', $user->id],'method' => 'put', 'class' => 'form-horizontal']) !!}
            <form class="form-horizontal">
              <div class="box-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                   @if(Session::has('alert-' . $msg))
                     <div class="flash-message">
                       <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                     </div>
                   @endif
                @endforeach
                <div class="form-group">
                  {{ Form::label('username', 'Username', array('class' => 'col-sm-2 control-label')) }}
                  <div class="col-sm-10">
                    {{ Form::text('username', old('username'), array('class' => 'form-control', 'disabled' => 'disabled')) }}
                  </div>
                </div>
                <div class="form-group">
                  {{ Form::label('first_name', 'First Name', array('class' => 'col-sm-2 control-label')) }}
                  <div class="col-sm-10 {{ $errors->has('first_name') ? ' has-error' : '' }}">
                    {{ Form::text('first_name', old('first_name'), array('class' => 'form-control')) }}
                  </div>
                </div>
                <div class="form-group">
                  {{ Form::label('last_name', 'Last Name', array('class' => 'col-sm-2 control-label')) }}
                  <div class="col-sm-10 {{ $errors->has('last_name') ? ' has-error' : '' }}">
                    {{ Form::text('last_name', old('last_name'), array('class' => 'form-control')) }}
                  </div>
                </div>
                <div class="form-group">
                  {{ Form::label('email', 'Email', array('class' => 'col-sm-2 control-label')) }}
                  <div class="col-sm-10 {{ $errors->has('email') ? ' has-error' : '' }}">
                    {{ Form::email('email', old('email'), array('class' => 'form-control')) }}
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{ URL::previous() }}" type="submit" class="btn btn-default">Back</a>
                {{ Form::submit('Update Profile', array('class' => 'btn btn-info pull-right')) }}
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
        </div>
      </div>
                <!-- END CONTENT BODY -->
@endsection

