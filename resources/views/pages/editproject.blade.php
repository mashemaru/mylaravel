@extends('layouts.app')

@section('htmlheader_title')
  Edit Project
@endsection

@section('content') <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Blank Page</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Page Layouts</span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> Edit Project 
                    </h3>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
        <div class="col-md-8">
          <div class="box box-info box-solid">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::model($project, ['route' => ['projects.update', $project->id],'method' => 'put', 'class' => 'form-horizontal']) !!}
            @include('pages.project.partials.projectforms', ['submitButton' => 'Update Project'])
            <!-- /form end -->
          </div>
        </div>
      </div>
                <!-- END CONTENT BODY -->
@endsection

