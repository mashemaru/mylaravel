
            <form class="form-horizontal">
              <div class="box-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                   @if(Session::has('alert-' . $msg))
                     <div class="flash-message">
                       <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                     </div>
                   @endif
                @endforeach
                <div class="form-group">
                  {{ Form::label('project_title', 'Project Title', array('class' => 'col-sm-2 control-label')) }}
                  <div class="col-sm-10 {{ $errors->has('project_title') ? ' has-error' : '' }}">
                    {{ Form::text('project_title', old('project_title'), array('class' => 'form-control')) }}
                  </div>
                </div>
                <div class="form-group">
                  {{ Form::label('description', 'Description', array('class' => 'col-sm-2 control-label')) }}
                  <div class="col-sm-10 {{ $errors->has('description') ? ' has-error' : '' }}">
                    {{ Form::textarea('description', old('description'), array('class' => 'form-control')) }}
                  </div>
                </div>
                <div class="form-group">
                  {{ Form::label('active', 'Active', array('class' => 'col-sm-2 control-label')) }}
                  <div class="col-sm-10 {{ $errors->has('active') ? ' has-error' : '' }}">
                    {{ Form::checkbox('active', null, old('active'), array('class' => 'form-control')) }}
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{ URL::previous() }}" type="submit" class="btn btn-default">Back</a>
                {{ Form::submit($submitButton, array('class' => 'btn btn-info pull-right')) }}
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!}