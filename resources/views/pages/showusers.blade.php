@extends('layouts.app')

@section('htmlheader_title')
  View Users
@endsection

@section('header')
  <meta name="_token" content="{!! csrf_token() !!}"/>
  <link href="//cdn.datatables.net/responsive/2.0.0/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Blank Page</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Page Layouts</span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> View All Users
                        <small> ( {{ Auth::user()->count() }} )</small>
                    </h3>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                   @if(Session::has('alert-' . $msg))
                     <div class="flash-message">
                       <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                     </div>
                   @endif
          @endforeach
    <div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="icon-settings font-dark"></i>
            <span class="caption-subject bold uppercase">Basic</span>
        </div>
        <div class="tools"> </div>
    </div>
    <div class="portlet-body">
          <table id="users-table" class="table table-striped table-bordered table-hover dt-responsive dataTable no-footer dtr-inline collapsed" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>ID</th>
              <th>Username</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>Status</th>
              <th width="16%"></th>
            </tr>
          </thead>
          </table>
    </div>
    </div>

</div>
        
                <!-- END CONTENT BODY -->
@endsection

@section('custom-scripts')
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/responsive/2.0.1/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/responsive/2.0.0/js/responsive.bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
  <script src="{{ url('../assets/custom/scripts/custom.min.js') }}" type="text/javascript"></script>
@endsection