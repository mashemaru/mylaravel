<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->

@section('htmlheader')
    @include('layouts.partials.htmlheader')
    @yield('header')
@show

  <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

      @include('layouts.partials.mainheader')

      <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
    <div class="page-container">  
        @include('layouts.partials.sidebar')

          <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
              <!-- BEGIN CONTENT BODY -->
                @yield('content')
              <!-- END CONTENT BODY -->
            </div>
          <!-- END CONTENT -->
    </div>
  <!-- END CONTAINER -->
    @include('layouts.partials.footer')

    @section('scripts')
        @include('layouts.partials.scripts')
          @foreach (['error', 'warning', 'success', 'info'] as $msg)
                  @if(Session::has($msg))
                      <script type="text/javascript">
                        @if($msg == 'danger')
                          toastr.error('{{Session::get($msg)}}');
                        @elseif($msg == 'warning')
                          toastr.warning('{{Session::get($msg)}}');
                        @elseif($msg == 'success')
                          toastr.success('{{Session::get($msg)}}');
                        @elseif($msg == 'info')
                          toastr.info('{{Session::get($msg)}}');
                        @endif
                      </script>
                  @endif
          @endforeach
          @if (session('status'))
              <script type="text/javascript">
                    toastr.success('{{session('status')}}');
              </script>
          @endif
        @yield('custom-scripts')
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ url('../assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('../assets/layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('../assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    @show
  </body>

</html>
