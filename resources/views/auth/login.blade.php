@extends('auth.loginapp')

@section('content')
<!-- BEGIN LOGIN FORM -->
                    {!! Form::open(array('url' => '/login', 'class' => 'login-form')) !!}
                    <h3 class="form-title font-green">Sign In</h3>
                      <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                      <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                      <label class="control-label visible-ie8 visible-ie9">Username</label>
                        {{ Form::text('username', old('username'), array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Username')) }}
                      </div>
                      <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                      <label class="control-label visible-ie8 visible-ie9">Password</label>
                        {{ Form::password('password', array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Password')) }}
                      </div>
                      <div class="form-actions">
                          <label class="rememberme check">
                            {{ Form::checkbox('remember', null) }} Remember Me</label>
                          {{ Form::submit('Login', array('class' => 'btn green uppercase pull-right')) }}
                      </div>
                      <div class="login-options">
                        <h4><a href="{{ url('/password/reset') }}" id="forget-password" class="forget-password">Forgot Password?</a></h4>
                      </div>
                      

                      <div class="create-account">
                          <p>
                              <a href="{{ url('/register') }}" id="register-btn" class="uppercase">Create an account</a>
                          </p>
                      </div>
                    {!! Form::close() !!}
            <!-- END LOGIN FORM -->
@endsection