@extends('auth.loginapp')

@section('htmlheader_title')
  Reset Password
@endsection

<!-- Main Content -->
@section('content')
<!-- BEGIN FORGOT PASSWORD FORM -->
            {!! Form::open(array('url' => '/password/email', 'class' => 'forget-form')) !!}
                <h3 class="font-green">Reset Password</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">E-Mail Address</label>
                    {{ Form::email('email', old('email'), array('class' => 'form-control placeholder-no-fix' , 'placeholder' => 'E-Mail Address')) }}
                </div>
                <div class="form-actions">
                    <a href="{{ url('/') }}" id="back-btn" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-success uppercase pull-right">Send Password Reset Link</button>
                </div>
            {!! Form::close() !!}
            <!-- END FORGOT PASSWORD FORM -->
@endsection
