@extends('auth.loginapp')

@section('htmlheader_title')
  Forgot Password
@endsection

@section('content')
<!-- BEGIN FORGOT PASSWORD FORM -->
            {!! Form::open(array('url' => '/password/reset', 'class' => 'forget-form')) !!}
                {{ Form::hidden('token', $token) }}
                <h3 class="font-green">Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">E-Mail Address</label>
                    {{ Form::email('email', old('email'), array('class' => 'form-control placeholder-no-fix' , 'placeholder' => 'E-Mail Address')) }}
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    {{ Form::password('password', array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Password')) }}
                </div>
                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
                    {{ Form::password('password_confirmation', array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Confirm Password')) }}
                </div>
                <div class="form-actions">
                    <a href="{{ url('/') }}" id="back-btn" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-success uppercase pull-right">Reset Password</button>
                </div>
            {!! Form::close() !!}
            <!-- END FORGOT PASSWORD FORM -->
@endsection