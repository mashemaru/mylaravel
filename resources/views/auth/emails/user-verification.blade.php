{{-- <h3>Hi {{ $first_name }}!</h3> Click here to verify your account {{ url('auth/verification/' . $verification_token)  }} --}}
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Verify Your Email Address</h2>

        <div>
        <h3>Hi {{ $first_name }}!</h3>
            Thanks for creating an account with xWag.
            Please follow the link below to verify your email address
            {{ url('auth/verification/' . $verification_token) }}.<br/>

        </div>

    </body>
</html>