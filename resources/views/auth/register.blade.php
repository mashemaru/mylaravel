@extends('auth.loginapp')

@section('htmlheader_title')
  Register
@endsection

@section('content')
<!-- BEGIN REGISTRATION FORM -->
            {!! Form::open(array('url' => '/register', 'class' => 'register-form')) !!}
                <h3 class="font-green">Sign Up</h3>
                <p class="hint"> Enter your personal details below: </p>
                <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">First Name</label>
                    {{ Form::text('first_name', old('first_name'), array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'First Name')) }}
                </div>
                <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Last Name</label>
                    {{ Form::text('last_name', old('last_name'), array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Last Name')) }}
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    {{ Form::email('email', old('email'), array('class' => 'form-control placeholder-no-fix' , 'placeholder' => 'Email')) }}
                </div>
                <p class="hint"> Enter your account details below: </p>
                <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    {{ Form::text('username', old('username'), array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Username')) }}
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    {{ Form::password('password', array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Password', 'id' => 'register_password')) }}
                </div>
                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
                    {{ Form::password('password_confirmation', array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Re-type Your Password')) }}
                </div>
                <div class="form-group margin-top-20 margin-bottom-20">
                    <label class="check">
                        {{ Form::checkbox('tnc', null) }}I agree to the
                        <a href="javascript:;"> Terms of Service </a> &
                        <a href="javascript:;"> Privacy Policy </a>
                    </label>
                </div>
                <div class="form-actions">
                    <a href="{{ url('/') }}" id="register-back-btn" class="btn btn-default">Back</a>
                    <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            {!! Form::close() !!}
            <!-- END REGISTRATION FORM -->
@endsection